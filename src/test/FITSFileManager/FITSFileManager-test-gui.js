// ----------------------------------------------------------------------------
// PixInsight JavaScript Runtime API - PJSR Version 1.0
// ----------------------------------------------------------------------------
// FITSFileManager-test-gui.js
// ----------------------------------------------------------------------------
//
// This file is part of FITSFileManager script version 1.6
//
// Copyright (c) 2012-2021 Jean-Marc Lugrin.
// Copyright (c) 2003-2021 Pleiades Astrophoto S.L.
//
// Redistribution and use in both source and binary forms, with or without
// modification, is permitted provided that the following conditions are met:
//
// 1. All redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
// 2. All redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// 3. Neither the names "PixInsight" and "Pleiades Astrophoto", nor the names
//    of their contributors, may be used to endorse or promote products derived
//    from this software without specific prior written permission. For written
//    permission, please contact info@pixinsight.com.
//
// 4. All products derived from this software, in any form whatsoever, must
//    reproduce the following acknowledgment in the end-user documentation
//    and/or other materials provided with the product:
//
//    "This product is based on software from the PixInsight project, developed
//    by Pleiades Astrophoto and its contributors (http://pixinsight.com/)."
//
//    Alternatively, if that is where third-party acknowledgments normally
//    appear, this acknowledgment must be reproduced in the product itself.
//
// THIS SOFTWARE IS PROVIDED BY PLEIADES ASTROPHOTO AND ITS CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL PLEIADES ASTROPHOTO OR ITS
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, BUSINESS
// INTERRUPTION; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; AND LOSS OF USE,
// DATA OR PROFITS) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

"use strict";


#include "PJSR-unit-tests-support.jsh"


#define VERSION "1.6.1-tests"

#define TITLE     "FITSFileManager"

// For debugging inside the gui
#define DEBUG true

#include "../../scripts/FITSFileManager/PJSR-logging.jsh"

#include "../../scripts/FITSFileManager/FITSFileManager-helpers.jsh"
#include "../../scripts/FITSFileManager/FITSFileManager-parameters.jsh"
#include "../../scripts/FITSFileManager/FITSFileManager-text.jsh"


#include <pjsr/Sizer.jsh>
//#include <pjsr/FrameStyle.jsh>
#include <pjsr/TextAlign.jsh>
#include <pjsr/StdIcon.jsh>
#include <pjsr/StdCursor.jsh>
#include <pjsr/StdButton.jsh>
#include <pjsr/FrameStyle.jsh>
#include <pjsr/Color.jsh>

#include <pjsr/ButtonCodes.jsh>
#include <pjsr/FocusStyle.jsh>



#include "../../scripts/FITSFileManager/FITSFileManager-config-gui.jsh"


#define TESTRULESETS


//

  // ========================================================================================================================
   // SUPPORT FOR TESTSTextEntryRow

var ffM_TestConfigUi = (function(){

   let addVariable = ffM_Configuration.addVariable;
   let defineVariable = ffM_Configuration.defineVariable;


   // Model object wrap data object and behavior
   let newConfigurationData = function(name, description) {
      let configuration = {
         name: name,
         description: description,
         variableList: [],
        builtins: {
           rank: {format: "%4.4d"},
           count: {format: "%4.4d"},
        }
      };
      let builder = {
         // Operations on the variable list
         addVariable: function(variable) {
            configuration.variableList.push(variable);
            return builder;
         },
         build: function() {
            return configuration;
         },
      }

      return  builder;

   }


   // --------------------------
   // Default definition for test
   let defaultRule = newConfigurationData('Default', 'Common FITS rules')
   .addVariable(defineVariable('type','Type of image (flat, bias, ...)','RegExpList'))
   .addVariable(defineVariable('filter','Filter (clear, red, ...)','RegExpList'))
   .addVariable(defineVariable('exposure','Exposure in seconds','Integer'))
   .addVariable(defineVariable('temp','Temperature in C','Integer'))
   .addVariable(defineVariable('binning','Binning as 1x1, 2x2, ...','IntegerPair'))
   .addVariable(defineVariable('night','night (experimental)','Night'))
   .addVariable(defineVariable('filename','Input file name','FileName'))
   .addVariable(defineVariable('extension','Input file extension','FileExtension'))
   .build();

#ifdef TESTRULESETS
   let testRule = newConfigurationData('Test', 'A test configuration')
   .addVariable(defineVariable('object','Object','Constant'))
   .build();

   let emptyRule = newConfigurationData('Empty', 'An empty configuration')
   .build();
   let largeRule = newConfigurationData('Large', 'Many variables')
   .addVariable(defineVariable('object 1','Object','Constant'))
   .addVariable(defineVariable('object 2','Object','Constant'))
   .addVariable(defineVariable('object 3','Object','Constant'))
   .addVariable(defineVariable('object 4','Object','Constant'))
   .addVariable(defineVariable('object 5','Object','Constant'))
   .addVariable(defineVariable('object 6','Object','Constant'))
   .addVariable(defineVariable('object 7','Object','Constant'))
   .addVariable(defineVariable('object 8','Object','Constant'))
   .addVariable(defineVariable('object 9','Object','Constant'))
   .addVariable(defineVariable('object 10','Object','Constant'))
   .addVariable(defineVariable('object 11','Object','Constant'))
   .addVariable(defineVariable('object 12','Object','Constant'))
   .addVariable(defineVariable('object 13','Object','Constant'))
   .addVariable(defineVariable('object 14','Object','Constant'))
   .addVariable(defineVariable('object 15','Object','Constant'))
   .addVariable(defineVariable('object 16','Object','Constant'))
   .addVariable(defineVariable('object 17','Object','Constant'))
   .addVariable(defineVariable('object 18','Object','Constant'))
   .addVariable(defineVariable('object 19','Object','Constant'))
   .addVariable(defineVariable('object 20','Object','Constant'))
   .build();

#endif


   // Test Rules
   let testConfigurations = [];
   testConfigurations.push(defaultRule);
#ifdef TESTRULESETS
   testConfigurations.push(testRule);
   testConfigurations.push(emptyRule);
   testConfigurations.push(largeRule);
#endif

   return {
      // For tests
      testConfigurations: testConfigurations,
   }
}) ();


function vP_testConfigurationGui()
{

   Console.show();
   Console.writeln("FITSFileManager-test-gui - Actions on the GUI will be logged on the console");

   let configurationSet = ffM_TestConfigUi.testConfigurations;
   // Note: this log takes forever...
   //Console.writeln("Initiale ruleset:\n" + Log.pp(configurationSet));

   // let names=[];
   // for (let i=0; i<configurationSet.length; i++) {
   //    names.push(configurationSet[i].name);
   // }
   // Make a fake guiParamters with a convenient test directory
   let scriptDir = getDirectoryOfFileWithDriveLetter(#__FILE__ ) ;
   let d = getDirectoryWithDriveLetter(scriptDir + "/../../data") ;
   let dialog =  ffM_GUI_config.makeDialog(null,{lastConfigurationDirectory: d});
   dialog.configure(configurationSet, configurationSet[0].name);
   for ( ;; )
   {
      let result = dialog.execute();
      debug("vP_testGuiRuleSet: Result", result);

      if (result) {

         // Writing to the console takes forever... be patient
         Console.writeln("vP_testGuiRuleSet: currentConfigurationName: " + Log.pp(dialog.currentConfigurationName,0,true));
         Console.writeln(Log.pp(dialog.editedConfigurationSet));
         Console.flush();
       }
       if (!result) break;

   }

   Console.writeln("FITSFileManager-test-gui - exiting");

}


vP_testConfigurationGui();
