// ----------------------------------------------------------------------------
// PixInsight JavaScript Runtime API - PJSR Version 1.0
// ----------------------------------------------------------------------------
// SigningKeysGUI.js - Released 2022-04-13T15:13:50Z
// ----------------------------------------------------------------------------
//
// This file is part of PixInsight Signing Keys Maintenance Utility Script 1.0
//
// Copyright (c) 2021-2022 Pleiades Astrophoto S.L.
//
// Redistribution and use in both source and binary forms, with or without
// modification, is permitted provided that the following conditions are met:
//
// 1. All redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
// 2. All redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// 3. Neither the names "PixInsight" and "Pleiades Astrophoto", nor the names
//    of their contributors, may be used to endorse or promote products derived
//    from this software without specific prior written permission. For written
//    permission, please contact info@pixinsight.com.
//
// 4. All products derived from this software, in any form whatsoever, must
//    reproduce the following acknowledgment in the end-user documentation
//    and/or other materials provided with the product:
//
//    "This product is based on software from the PixInsight project, developed
//    by Pleiades Astrophoto and its contributors (https://pixinsight.com/)."
//
//    Alternatively, if that is where third-party acknowledgments normally
//    appear, this acknowledgment must be reproduced in the product itself.
//
// THIS SOFTWARE IS PROVIDED BY PLEIADES ASTROPHOTO AND ITS CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL PLEIADES ASTROPHOTO OR ITS
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, BUSINESS
// INTERRUPTION; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; AND LOSS OF USE,
// DATA OR PROFITS) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/*
 * PixInsight Signing Keys Maintenance Utility
 *
 * Copyright (C) 2021-2022 Pleiades Astrophoto. All Rights Reserved.
 * Written by Juan Conejero (PTeam)
 *
 * Graphical user interface
 */

#include <pjsr/FocusStyle.jsh>
#include <pjsr/FrameStyle.jsh>
#include <pjsr/Sizer.jsh>
#include <pjsr/StdButton.jsh>
#include <pjsr/StdDialogCode.jsh>
#include <pjsr/StdIcon.jsh>
#include <pjsr/TextAlign.jsh>

/*
 * Signing Keys Maintenance Dialog
 */
function SigningKeysDialog()
{
   this.__base__ = Dialog;
   this.__base__();

   //

   let emWidth = this.font.width( 'M' );
   let labelWidth1 = this.font.width( "Developer:" ) + 2*emWidth;

   //

   this.helpLabel = new Label( this );
   this.helpLabel.styleSheet = this.scaledStyleSheet(
      "QWidget#" + this.helpLabel.uniqueId + " {"
   +     "border: 1px solid gray;"
   +     "padding: 0.25em;"
   +  "}" );
   this.helpLabel.wordWrapping = true;
   this.helpLabel.useRichText = true;
   this.helpLabel.text = "<p><strong>" + TITLE + " version " + VERSION + "</strong><br/>" +
                         "Copyright &copy; 2021-2022 Pleiades Astrophoto. All Rights Reserved.</p>";
   //

   let developerId_ToolTip = "<p>Your Certified PixInsight Developer (CPD) identifier.</p>"

   this.developerId_Label = new Label( this );
   this.developerId_Label.text = "Developer:";
   this.developerId_Label.minWidth = labelWidth1;
   this.developerId_Label.textAlignment = TextAlign_Right|TextAlign_VertCenter;
   this.developerId_Label.toolTip = developerId_ToolTip;

   this.developerId_Edit = new Edit( this );
   this.developerId_Edit.toolTip = developerId_ToolTip;
   this.developerId_Edit.onEditCompleted = function()
   {
      let developerId = this.text.trim();
      this.text = developerId;
      g_workingData.developerId = developerId;
   };

   this.developerIdClear_Button = new ToolButton( this );
   this.developerIdClear_Button.icon = this.scaledResource( ":/icons/clear.png" );
   this.developerIdClear_Button.setScaledFixedSize( 22, 22 );
   this.developerIdClear_Button.focusStyle = FocusStyle_NoFocus;
   this.developerIdClear_Button.toolTip = "<p>Clear the Developer field.</p>";
   this.developerIdClear_Button.onClick = function()
   {
      g_workingData.developerId = this.dialog.developerId_Edit.text = "";
      this.dialog.developerId_Edit.hasFocus = true;
   };

   this.developerId_Sizer = new HorizontalSizer;
   this.developerId_Sizer.spacing = 4;
   this.developerId_Sizer.add( this.developerId_Label );
   this.developerId_Sizer.add( this.developerId_Edit, 100 );
   this.developerId_Sizer.add( this.developerIdClear_Button );

   //

   this.localSigningIdentity_CheckBox = new CheckBox( this );
   this.localSigningIdentity_CheckBox.text = "Local signing identity";
   this.localSigningIdentity_CheckBox.toolTip =
      "<p>Enable this option to generate a signing keys file for the local signing identity.</p>" +
      "<p>The local signing identity is linked to your software license, and can only be used on machines " +
      "where you have valid PixInsight license files.</p>";
   this.localSigningIdentity_CheckBox.onClick = function( checked )
   {
      g_workingData.localSigningIdentity = checked;
      this.dialog.updateControls();
   };

   this.localSigningIdentity_Sizer = new HorizontalSizer;
   this.localSigningIdentity_Sizer.addUnscaledSpacing( labelWidth1 + this.logicalPixelsToPhysical( 4 ) );
   this.localSigningIdentity_Sizer.add( this.localSigningIdentity_CheckBox );
   this.localSigningIdentity_Sizer.addStretch();

   //

   this.generateKeysFile_GroupBox = new GroupBox( this );
   this.generateKeysFile_GroupBox.title = "Generate Signing Keys";
   this.generateKeysFile_GroupBox.titleCheckBox = true;
   this.generateKeysFile_GroupBox.sizer = new VerticalSizer;
   this.generateKeysFile_GroupBox.sizer.margin = 8;
   this.generateKeysFile_GroupBox.sizer.spacing = 8;
   this.generateKeysFile_GroupBox.sizer.add( this.developerId_Sizer );
   this.generateKeysFile_GroupBox.sizer.add( this.localSigningIdentity_Sizer );
   this.generateKeysFile_GroupBox.onCheck = function( checked )
   {
      g_workingData.exportKeysFile = !checked;
      this.dialog.updateControls();
   };

   //

   let inputFile_ToolTip = "<p>The signing keys file with the public and private keys that will be exported.</p>";

   this.inputFile_Label = new Label( this );
   this.inputFile_Label.text = "Keys file:";
   this.inputFile_Label.minWidth = labelWidth1;
   this.inputFile_Label.textAlignment = TextAlign_Right|TextAlign_VertCenter;
   this.inputFile_Label.toolTip = inputFile_ToolTip;

   this.inputFile_Edit = new Edit( this );
   this.inputFile_Edit.toolTip = inputFile_ToolTip;
   this.inputFile_Edit.onEditCompleted = function()
   {
      let filePath = File.windowsPathToUnix( this.text.trim() );
      this.text = filePath;
      g_workingData.inputFilePath = filePath;
   };

   this.inputFileSelect_Button = new ToolButton( this );
   this.inputFileSelect_Button.icon = this.scaledResource( ":/icons/select-file.png" );
   this.inputFileSelect_Button.setScaledFixedSize( 22, 22 );
   this.inputFileSelect_Button.toolTip = "<p>Select the input signing keys file.</p>";
   this.inputFileSelect_Button.onClick = function()
   {
      let ofd = new OpenFileDialog;
      ofd.multipleSelections = false;
      ofd.caption = "Select Signing Keys File";
      ofd.filters = [ ["Secure signing keys files", "*.xssk"] ];
      if ( ofd.execute() )
         this.dialog.inputFile_Edit.text = g_workingData.inputFilePath = ofd.fileNames[0];
   };

   this.inputFile_Sizer = new HorizontalSizer;
   this.inputFile_Sizer.spacing = 4;
   this.inputFile_Sizer.add( this.inputFile_Label );
   this.inputFile_Sizer.add( this.inputFile_Edit, 100 );
   this.inputFile_Sizer.add( this.inputFileSelect_Button );

   //

   let inputPassword_ToolTip = "<p>Secure signing keys files (.xssk) are password-protected. " +
      "Enter here the password that protects the keys file that you have specified to export signing keys.</p>";

   this.inputPassword_Label = new Label( this );
   this.inputPassword_Label.text = "Password:";
   this.inputPassword_Label.minWidth = labelWidth1;
   this.inputPassword_Label.textAlignment = TextAlign_Right|TextAlign_VertCenter;
   this.inputPassword_Label.toolTip = inputPassword_ToolTip;

   this.inputPassword_Edit = new Edit( this );
   this.inputPassword_Edit.passwordMode = true;
   this.inputPassword_Edit.setScaledFixedWidth( 20*emWidth );
   this.inputPassword_Edit.toolTip = inputPassword_ToolTip;
   this.inputPassword_Edit.onEditCompleted = function()
   {
      this.text = this.text.trim();
   };

   this.inputPasswordHide_Button = new ToolButton( this );
   this.inputPasswordHide_Button.checkable = true;
   this.inputPasswordHide_Button.checked = true;
   this.inputPasswordHide_Button.icon = this.scaledResource( ":/icons/control-password.png" );
   this.inputPasswordHide_Button.setScaledFixedSize( 22, 22 );
   this.inputPasswordHide_Button.focusStyle = FocusStyle_NoFocus;
   this.inputPasswordHide_Button.toolTip = "<p>Hide the password.</p>";
   this.inputPasswordHide_Button.onClick = function( checked )
   {
      this.dialog.inputPassword_Edit.passwordMode = checked;
   };

   this.inputPasswordClear_Button = new ToolButton( this );
   this.inputPasswordClear_Button.icon = this.scaledResource( ":/icons/clear.png" );
   this.inputPasswordClear_Button.setScaledFixedSize( 22, 22 );
   this.inputPasswordClear_Button.focusStyle = FocusStyle_NoFocus;
   this.inputPasswordClear_Button.toolTip = "<p>Clear the Password field.</p>";
   this.inputPasswordClear_Button.onClick = function()
   {
      this.dialog.inputPassword_Edit.text = "";
      this.dialog.inputPassword_Edit.hasFocus = true;
   };

   this.inputPassword_Sizer = new HorizontalSizer;
   this.inputPassword_Sizer.spacing = 4;
   this.inputPassword_Sizer.add( this.inputPassword_Label );
   this.inputPassword_Sizer.add( this.inputPassword_Edit, 100 );
   this.inputPassword_Sizer.add( this.inputPasswordHide_Button );
   this.inputPassword_Sizer.add( this.inputPasswordClear_Button );
   this.inputPassword_Sizer.addStretch();

   //

   this.exportKeysFile_GroupBox = new GroupBox( this );
   this.exportKeysFile_GroupBox.title = "Export Signing Keys";
   this.exportKeysFile_GroupBox.titleCheckBox = true;
   this.exportKeysFile_GroupBox.sizer = new VerticalSizer;
   this.exportKeysFile_GroupBox.sizer.margin = 8;
   this.exportKeysFile_GroupBox.sizer.spacing = 8;
   this.exportKeysFile_GroupBox.sizer.add( this.inputFile_Sizer );
   this.exportKeysFile_GroupBox.sizer.add( this.inputPassword_Sizer );
   this.exportKeysFile_GroupBox.onCheck = function( checked )
   {
      g_workingData.exportKeysFile = checked;
      this.dialog.updateControls();
   };

   //

   let outputFile_ToolTip = "<p>The signing keys file that will be generated with new public and private keys.</p>";

   this.outputFile_Label = new Label( this );
   this.outputFile_Label.text = "Keys file:";
   this.outputFile_Label.minWidth = labelWidth1;
   this.outputFile_Label.textAlignment = TextAlign_Right|TextAlign_VertCenter;
   this.outputFile_Label.toolTip = outputFile_ToolTip;

   this.outputFile_Edit = new Edit( this );
   this.outputFile_Edit.toolTip = outputFile_ToolTip;
   this.outputFile_Edit.onEditCompleted = function()
   {
      let filePath = File.windowsPathToUnix( this.text.trim() );
      this.text = filePath;
      g_workingData.outputFilePath = filePath;
   };

   this.outputFileSelect_Button = new ToolButton( this );
   this.outputFileSelect_Button.icon = this.scaledResource( ":/icons/select-file.png" );
   this.outputFileSelect_Button.setScaledFixedSize( 22, 22 );
   this.outputFileSelect_Button.toolTip = "<p>Select the output signing keys file.</p>";
   this.outputFileSelect_Button.onClick = function()
   {
      let sfd = new SaveFileDialog;
      sfd.caption = "Select Signing Keys File";
      sfd.filters = [ ["Secure signing keys files", "*.xssk"] ];
      if ( sfd.execute() )
         this.dialog.outputFile_Edit.text = g_workingData.outputFilePath = sfd.fileName;
   };

   this.outputFile_Sizer = new HorizontalSizer;
   this.outputFile_Sizer.spacing = 4;
   this.outputFile_Sizer.add( this.outputFile_Label );
   this.outputFile_Sizer.add( this.outputFile_Edit, 100 );
   this.outputFile_Sizer.add( this.outputFileSelect_Button );

   //

   let outputPassword_ToolTip = "<p>Secure signing keys files (.xssk) are password-protected. " +
      "Enter here the password that will protect the new keys file that will be generated.</p>";

   this.outputPassword_Label = new Label( this );
   this.outputPassword_Label.text = "Password:";
   this.outputPassword_Label.minWidth = labelWidth1;
   this.outputPassword_Label.textAlignment = TextAlign_Right|TextAlign_VertCenter;
   this.outputPassword_Label.toolTip = outputPassword_ToolTip;

   this.outputPassword_Edit = new Edit( this );
   this.outputPassword_Edit.passwordMode = true;
   this.outputPassword_Edit.setScaledFixedWidth( 20*emWidth );
   this.outputPassword_Edit.toolTip = outputPassword_ToolTip;
   this.outputPassword_Edit.onEditCompleted = function()
   {
      this.text = this.text.trim();
   };

   this.outputPasswordHide_Button = new ToolButton( this );
   this.outputPasswordHide_Button.checkable = true;
   this.outputPasswordHide_Button.checked = true;
   this.outputPasswordHide_Button.icon = this.scaledResource( ":/icons/control-password.png" );
   this.outputPasswordHide_Button.setScaledFixedSize( 22, 22 );
   this.outputPasswordHide_Button.focusStyle = FocusStyle_NoFocus;
   this.outputPasswordHide_Button.toolTip = "<p>Hide the password.</p>";
   this.outputPasswordHide_Button.onClick = function( checked )
   {
      this.dialog.outputPassword_Edit.passwordMode = checked;
   };

   this.outputPasswordClear_Button = new ToolButton( this );
   this.outputPasswordClear_Button.icon = this.scaledResource( ":/icons/clear.png" );
   this.outputPasswordClear_Button.setScaledFixedSize( 22, 22 );
   this.outputPasswordClear_Button.focusStyle = FocusStyle_NoFocus;
   this.outputPasswordClear_Button.toolTip = "<p>Clear the Password field.</p>";
   this.outputPasswordClear_Button.onClick = function()
   {
      this.dialog.outputPassword_Edit.text = "";
      this.dialog.outputPassword_Edit.hasFocus = true;
   };

   this.outputPassword_Sizer = new HorizontalSizer;
   this.outputPassword_Sizer.spacing = 4;
   this.outputPassword_Sizer.add( this.outputPassword_Label );
   this.outputPassword_Sizer.add( this.outputPassword_Edit, 100 );
   this.outputPassword_Sizer.add( this.outputPasswordHide_Button );
   this.outputPassword_Sizer.add( this.outputPasswordClear_Button );
   this.outputPassword_Sizer.addStretch();

   //

   this.output_GroupBox = new GroupBox( this );
   this.output_GroupBox.title = "Output";
   this.output_GroupBox.sizer = new VerticalSizer;
   this.output_GroupBox.sizer.margin = 8;
   this.output_GroupBox.sizer.spacing = 8;
   this.output_GroupBox.sizer.add( this.outputFile_Sizer );
   this.output_GroupBox.sizer.add( this.outputPassword_Sizer );

   //

   this.reset_Button = new PushButton( this );
   this.reset_Button.text = "Reset";
   this.reset_Button.icon = this.scaledResource( ":/icons/reload.png" );
   this.reset_Button.onClick = function()
   {
      g_workingData.reset();
      g_workingData.saveSettings();
      this.dialog.updateControls();
   };

   this.run_Button = new PushButton( this );
   this.run_Button.defaultButton = true;
   this.run_Button.text = "Run";
   this.run_Button.icon = this.scaledResource( ":/icons/power.png" );
   this.run_Button.onClick = function()
   {
      this.dialog.ok();
   };

   this.exit_Button = new PushButton( this );
   this.exit_Button.text = "Exit";
   this.exit_Button.icon = this.scaledResource( ":/icons/close.png" );
   this.exit_Button.onClick = function()
   {
      this.dialog.cancel();
   };

   this.buttons_Sizer = new HorizontalSizer;
   this.buttons_Sizer.spacing = 8;
   this.buttons_Sizer.add( this.reset_Button );
   this.buttons_Sizer.addStretch();
   this.buttons_Sizer.add( this.run_Button );
   this.buttons_Sizer.add( this.exit_Button );

   //

   this.sizer = new VerticalSizer;
   this.sizer.margin = 8;
   this.sizer.spacing = 8;
   this.sizer.add( this.helpLabel );
   this.sizer.addSpacing( 4 );
   this.sizer.add( this.generateKeysFile_GroupBox );
   this.sizer.add( this.exportKeysFile_GroupBox );
   this.sizer.add( this.output_GroupBox );
   this.sizer.add( this.buttons_Sizer );

   this.windowTitle = TITLE;

   this.ensureLayoutUpdated();
   this.adjustToContents();
   this.setFixedSize();

   //

   this.updateControls = function()
   {
      this.generateKeysFile_GroupBox.checked = !g_workingData.exportKeysFile;

      this.developerId_Label.enabled = !g_workingData.exportKeysFile && !g_workingData.localSigningIdentity;

      this.developerId_Edit.text = g_workingData.developerId;
      this.developerId_Edit.enabled = !g_workingData.exportKeysFile && !g_workingData.localSigningIdentity;

      this.localSigningIdentity_CheckBox.checked = g_workingData.localSigningIdentity;

      this.exportKeysFile_GroupBox.checked = g_workingData.exportKeysFile;

      this.inputFile_Edit.text = g_workingData.inputFilePath;

      this.outputFile_Edit.text = g_workingData.outputFilePath;
   };

   this.updateControls();
}

SigningKeysDialog.prototype = new Dialog;

// ----------------------------------------------------------------------------
// EOF SigningKeysGUI.js - Released 2022-04-13T15:13:50Z
